package com.automationanywhere.botcommand.samples.commands.basic;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class ResizeWithMaxPixelCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(ResizeWithMaxPixelCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    ResizeWithMaxPixel command = new ResizeWithMaxPixel();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sourceString") && parameters.get("sourceString") != null && parameters.get("sourceString").get() != null) {
      convertedParameters.put("sourceString", parameters.get("sourceString").get());
      if(convertedParameters.get("sourceString") !=null && !(convertedParameters.get("sourceString") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sourceString", "String", parameters.get("sourceString").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sourceString") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sourceString"));
    }

    if(parameters.containsKey("targetString") && parameters.get("targetString") != null && parameters.get("targetString").get() != null) {
      convertedParameters.put("targetString", parameters.get("targetString").get());
      if(convertedParameters.get("targetString") !=null && !(convertedParameters.get("targetString") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","targetString", "String", parameters.get("targetString").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("targetString") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","targetString"));
    }

    if(parameters.containsKey("targetMax") && parameters.get("targetMax") != null && parameters.get("targetMax").get() != null) {
      convertedParameters.put("targetMax", parameters.get("targetMax").get());
      if(convertedParameters.get("targetMax") !=null && !(convertedParameters.get("targetMax") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","targetMax", "String", parameters.get("targetMax").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("targetMax") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","targetMax"));
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sourceString"),(String)convertedParameters.get("targetString"),(String)convertedParameters.get("targetMax")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
