/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.samples.commands.basic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 *<pre>
	Check the size of image in pixel and return to Bot.
 * </pre>

 *
 * @author Takehiko Isono
 */

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "ResizeNewSize", label = "[[ResizeNewSize.label]]",
		node_label = "[[ResizeNewSize.node_label]]",  description = "[[ResizeNewSize.description]]", icon = "pkg.svg",
		
		//Return type information. return_type ensures only the right kind of variable is provided on the UI. 
		return_label = "[[ResizeNewSize.return_label]]", return_type = STRING, return_required = true)

public class ResizeNewSize {
	//Messages read from full qualified property file name and provide i18n capability.
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public Value<String> action(
			//Idx 1  Image file path
			@Idx(index = "1", type = FILE)
			@Pkg(label = "[[ResizeNewSize.sourceString.label]]")
			@NotEmpty
			String sourceString,

			//Idx 2  Output file path
			@Idx(index = "2", type = TEXT)
			@Pkg(label = "[[ResizeNewSize.targetString.label]]")
			@NotEmpty
					String targetString,

			//Idx 3  Target Width
			@Idx(index = "3", type = TEXT)
			@Pkg(label = "[[ResizeNewSize.Width.label]]")
			//@NotEmpty
			String targetWidth,

			//Idx 4  Target Height
			@Idx(index = "4", type = TEXT)
			@Pkg(label = "[[ResizeNewSize.Height.label]]")
			//@NotEmpty
					String targetHeight

			)
	{
		
		//Internal validation, to disallow empty strings. No null check needed as we have NotEmpty on firstString.
		if ("".equals(sourceString.trim()))
			throw new BotCommandException("Please select image file.");
		if ("".equals(targetString.trim()))
			throw new BotCommandException("Please specify output file path.");
		if ("".equals(targetWidth.trim()) && "".equals(targetHeight.trim()))
			throw new BotCommandException("Please specify target Width");

		//Business logic
		try {

			//target size
			int int_width;
			int int_height;

			//Check the size of original image
			BufferedImage InputImage = ImageIO.read(new File(sourceString));
			int original_w = InputImage.getWidth();
			int original_h = InputImage.getHeight();

			//Check if user entered either width or height.
			if (!"".equals(targetWidth.trim()) && "".equals(targetHeight.trim())){
				int_width = Integer.parseInt(targetWidth);
				int_height = (int) original_h * int_width / original_w;
			}else if ("".equals(targetWidth.trim()) && !"".equals(targetHeight.trim())){
				int_height = Integer.parseInt(targetHeight);
				int_width = (int) original_w * int_height / original_h;
			}else{
				int_width = Integer.parseInt(targetWidth);
				int_height = Integer.parseInt(targetHeight);
			}


			BufferedImage SourceImage = ImageIO.read(new File(sourceString));
			BufferedImage OutputImage = new BufferedImage(int_width, int_height, BufferedImage.TYPE_3BYTE_BGR);
			OutputImage.createGraphics().drawImage(SourceImage.getScaledInstance(
					int_width, int_height, Image.SCALE_AREA_AVERAGING)
					,0, 0, int_width, int_height, null);

			//Check extension of image
			String ext = sourceString.substring(sourceString.lastIndexOf(".")+1,sourceString.length());
			ImageIO.write(OutputImage,ext,new File(targetString));

		} catch (IOException e) {
			e.printStackTrace();
			throw new BotCommandException("Error converting size of image.");
		}

		//Return StringValue.
		return new StringValue("Success");
	}
}
