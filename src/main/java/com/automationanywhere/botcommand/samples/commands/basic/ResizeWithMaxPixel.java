/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.samples.commands.basic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 *<pre>
	Check the size of image in pixel and return to Bot.
 * </pre>

 *
 * @author Takehiko Isono
 */

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "ResizeWithMaxPixel", label = "[[ResizeWithMaxPixel.label]]",
		node_label = "[[ResizeWithMaxPixel.node_label]]",  description = "[[ResizeWithMaxPixel.description]]", icon = "pkg.svg",
		
		//Return type information. return_type ensures only the right kind of variable is provided on the UI. 
		return_label = "[[ResizeWithMaxPixel.return_label]]", return_type = STRING, return_required = true)

public class ResizeWithMaxPixel {
	//Messages read from full qualified property file name and provide i18n capability.
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public Value<String> action(
			//Idx 1  Image file path
			@Idx(index = "1", type = FILE)
			@Pkg(label = "[[ResizeWithMaxPixel.sourceString.label]]")
			@NotEmpty
			String sourceString,

			//Idx 2  Output file path
			@Idx(index = "2", type = TEXT)
			@Pkg(label = "[[ResizeWithMaxPixel.targetString.label]]")
			@NotEmpty
					String targetString,

			//Idx 3  Target Width percentage
			@Idx(index = "3", type = TEXT)
			@Pkg(label = "[[ResizeWithMaxPixel.Max.label]]")
			@NotEmpty
			String targetMax

			)
	{
		
		//Internal validation, to disallow empty strings. No null check needed as we have NotEmpty on firstString.
		if ("".equals(sourceString.trim()))
			throw new BotCommandException("Please select image file.");
		if ("".equals(targetString.trim()))
			throw new BotCommandException("Please specify output file path.");
		if ("".equals(targetMax.trim()))
			throw new BotCommandException("Please specify maximum pixel value.");

		//Business logic
		try {
			//target size
			int int_width;
			int int_height;

			//Check the size of original image
			BufferedImage InputImage = ImageIO.read(new File(sourceString));
			int original_w = InputImage.getWidth();
			int original_h = InputImage.getHeight();

			//Check if user entered either width or height.
			if (original_w > original_h){
				int_width = Integer.parseInt(targetMax);
				int_height = (int) original_h * int_width / original_w;
			}else{
				int_height = Integer.parseInt(targetMax);
				int_width = (int) original_w * int_height / original_h;
			}

			BufferedImage SourceImage = ImageIO.read(new File(sourceString));
			BufferedImage OutputImage = new BufferedImage(int_width, int_height, BufferedImage.TYPE_3BYTE_BGR);
			OutputImage.createGraphics().drawImage(SourceImage.getScaledInstance(
					int_width, int_height, Image.SCALE_AREA_AVERAGING)
					,0, 0, int_width, int_height, null);

			//Check extension of image
			String ext = sourceString.substring(sourceString.lastIndexOf(".")+1,sourceString.length());
			ImageIO.write(OutputImage,ext,new File(targetString));

		} catch (IOException e) {
			e.printStackTrace();
			throw new BotCommandException("Error converting size of image.");
		}

		//Return StringValue.
		return new StringValue("Success");
	}
}
