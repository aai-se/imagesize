/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.samples.commands.basic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 *<pre>
	Convert color image into Black/White with threshold.
 * </pre>

 *
 * @author Takehiko Isono
 */

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "ConvertBW", label = "[[ConvertBW.label]]",
		node_label = "[[ConvertBW.node_label]]",  description = "[[ConvertBW.description]]", icon = "pkg.svg",
		
		//Return type information. return_type ensures only the right kind of variable is provided on the UI. 
		return_label = "[[ConvertBW.return_label]]", return_type = STRING, return_required = true)

public class ConvertBW {
	//Messages read from full qualified property file name and provide i18n capability.
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public Value<String> action(
			//Idx 1  Image file path
			@Idx(index = "1", type = FILE)
			@Pkg(label = "[[ConvertBW.sourceString.label]]")
			@NotEmpty
			String sourceString,

			//Idx 2  Output file path
			@Idx(index = "2", type = TEXT)
			@Pkg(label = "[[ConvertBW.targetString.label]]")
			@NotEmpty
					String targetString,

			//Idx 3  Threshold
			@Idx(index = "3", type = TEXT)
			@Pkg(label = "[[ConvertBW.Threshold.label]]")
			//@NotEmpty
			String targetThreshold

			)
	{
		
		//Internal validation, to disallow empty strings. No null check needed as we have NotEmpty on firstString.
		if ("".equals(sourceString.trim()))
			throw new BotCommandException("Please select image file.");
		if ("".equals(targetString.trim()))
			throw new BotCommandException("Please specify output file path.");
		if ("".equals(targetThreshold.trim()) )
			throw new BotCommandException("Please specify threshold.");

		//Business logic
		try {

			int threshold = Integer.parseInt(targetThreshold);

			//Load original image
			BufferedImage InputImage = ImageIO.read(new File(sourceString));

			int int_width = InputImage.getWidth();
			int int_height = InputImage.getHeight();

			//Convert to Grayscale
			BufferedImage GrayImage = new BufferedImage(int_width, int_height,BufferedImage.TYPE_BYTE_GRAY);
			GrayImage.getGraphics().drawImage(InputImage,0,0,null);

/**
			if (InputImage.getType() == BufferedImage.TYPE_3BYTE_BGR){
				//Convert Color to BW.
				int color, r,g,b;

				for(int y=0;y<int_height;++y)
				{
					for(int x=0;x<int_width;++x){
						color = (InputImage.getRGB(x,y));
						r = (color >> 16) & 0xff;
						g= (color >>8) & 0xff;
						b = color& 0xff;

						if (threshold <= (r+g+b)/3){
							r = 255;
							g = 255;
							b = 255;
							InputImage.setRGB(x,y, (r<<16) + (g<<8)+b);
						}else{
							r = 0;
							g = 0;
							b = 0;
							InputImage.setRGB(x,y, (r<<16) + (g<<8)+b);
						}
					}

				}
			}else if(InputImage.getType() == BufferedImage.TYPE_BYTE_GRAY){
				//Convert Gray to BW.
				int color, r,g,b;

				for(int y=0;y<int_height;++y)
				{
					for(int x=0;x<int_width;++x){
						color = InputImage.getRGB(x,y) & 0xff;

						if (threshold <= color){
							InputImage.setRGB(x,y, -1);
						}else{
							InputImage.setRGB(x,y, 0);
						}
					}

				}

			}
			else {
				return new StringValue ("Error file format" + InputImage.getType());
			}
			//Check extension of image
			String ext = sourceString.substring(sourceString.lastIndexOf(".")+1,sourceString.length());
			ImageIO.write(InputImage,ext,new File(targetString));
 **/

			int color, r,g,b;

			for(int y=0;y<int_height;++y)
			{
				for(int x=0;x<int_width;++x){
					color = GrayImage.getRGB(x,y) & 0xff;

					if (threshold <= color){
						GrayImage.setRGB(x,y, -1);
					}else{
						GrayImage.setRGB(x,y, 0);
					}
				}

			}

			String ext = sourceString.substring(sourceString.lastIndexOf(".")+1,sourceString.length());
			ImageIO.write(GrayImage,ext,new File(targetString));

		} catch (IOException e) {
			e.printStackTrace();
			throw new BotCommandException("Error converting size of image.");
		}

		//Return StringValue.
		return new StringValue("Success");
	}
}
