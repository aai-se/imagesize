/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.samples.commands.basic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.FILE;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 *<pre>
	Check the size of image in pixel and return to Bot.
 * </pre>

 *
 * @author Takehiko Isono
 */

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "CheckSize", label = "[[Checksize.label]]",
		node_label = "[[Checksize.node_label]]",  description = "[[Checksize.description]]", icon = "pkg.svg",
		
		//Return type information. return_type ensures only the right kind of variable is provided on the UI. 
		return_label = "[[Checksize.return_label]]", return_type = STRING, return_required = true)

public class CheckSize {
	//Messages read from full qualified property file name and provide i18n capability.
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public Value<String> action(
			//Idx 1  Image file path
			@Idx(index = "1", type = AttributeType.FILE)
			@Pkg(label = "[[Checksize.sourceString.label]]")
			@NotEmpty
			String sourceString
			)
	{
		
		//Internal validation, to disallow empty strings. No null check needed as we have NotEmpty on firstString.
		if ("".equals(sourceString.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "sourceString"));

		int h;
		int w;
		//Business logic
		try {
			BufferedImage InputImage = ImageIO.read(new File(sourceString));
			h = InputImage.getHeight();
			w = InputImage.getWidth();

		} catch (IOException e) {
			e.printStackTrace();
			throw new BotCommandException("Error checking size of image.");
		}

		//Return StringValue.
		return new StringValue(w + "x" + h);
	}
}
